from pydantic import BaseSettings
import os

class Settings(BaseSettings):
    API_V1: str = "/api/v1"
    PROJECT_NAME: str = os.getenv('PROJECT_NAME', 'Food Recommendation')

settings = Settings()